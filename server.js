const express = require('express');
const expressGraphql = require('express-graphql');
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
    GraphQLNonNull
} = require('graphql');

const app = express();

//data to act as our database
const counties = [
    { id: 001, name: "Mombasa" },
    { id: 002, name: "Kwale" },
    { id: 003, name: "Kilifi" },
    { id: 004, name: "TanaRiver "},
    { id: 005, name: "Lamu" }
]

const constituencies = [
    {id: 001, constituency: "Kisauni", population: 194065 },
    {id: 001, constituency: "Nyali", population: 185990 },
    {id: 004, constituency: "Bangale", population: 14853 },
    {id: 004, constituency: "Garsen", population: 51592 },
    {id: 002, constituency: "Msambweni", population: 211567 },
    {id: 005, constituency: "Witu", population: 5967 },
    {id: 003, constituency: "Ganze", population: 137234},
]

// const schema = new GraphQLSchema({
//     query: new GraphQLObjectType({
//         name: 'HelloWorld',
//         fields: () => ({
//             message : { 
//             type: GraphQLString,
//             resolve: () => 'Hello World' //function to tell graphql where to get the info from
//             }
//         })
//     })
// });

const constituencyType = new GraphQLObjectType({
    name: 'Constituencies',
    description:'This represents a constituency',
    fields: () => ({//we use this function that an object so that all types are defined before they are called
        id: { type: GraphQLNonNull(GraphQLInt)},
        constituency: { type: GraphQLNonNull(GraphQLString)},
        population: { type: GraphQLNonNull(GraphQLString)},
        county: {
            type: countiesType,
            resolve: (constituencies) => {
               return counties.find(county => county.id === constituencies.id)//to get counties whose ids match
            }
        }
    })
})

const countiesType = new GraphQLObjectType({
    name: 'Counties',
    description:'This represents a county',
    fields: () => ({
        id: { type: GraphQLNonNull(GraphQLInt)},
        name: { type: GraphQLNonNull(GraphQLString)},
        constituencies: {
            type: GraphQLList(constituencyType),
            resolve: (counties) => {
                return constituencies.filter(constituency => constituency.id === counties.id)
            }
        }
    })
})
//root query
const rootQueryType = new GraphQLObjectType({
    name: 'Query',
    description: 'Root query',
    fields: () => ({
       
        constituencies: {
            type: new GraphQLList(constituencyType),
            description: 'List of all constituencies',
            resolve: () => constituencies
        },

        counties: {
            type: new GraphQLList(countiesType),
            description: 'List of counties',
            resolve: () => counties
        },

         //to get a single county
         county: {
            type: countiesType,
            description: 'A single county',
            args: {
                id: {type: GraphQLInt}
            },
            resolve: (parent, args) => counties.find(county => county.id === args.id)
        },
        //to get a single constituency
        constituency: {
            type: constituencyType,
            description: 'A single constituency',
            args: {
                id: { type: GraphQLInt }
            },
            resolve: (parent, args) => constituencies.find(constituency => constituency.id === args.id)
        }


    })
})

const rootMutationType = new GraphQLObjectType({
    name: 'Mutation',
    description:'Root Mutation',
    fields: () => ({
        addCounty: {
            type: countiesType,
            description: 'Add a county',
            args: {
                name: { type: GraphQLNonNull(GraphQLString) },
                id: { type: GraphQLNonNull(GraphQLInt) }
            },
            resolve: (parent, args) => {
                const county = {id: counties.length + 1, name: args.name, id: args.id}
                counties.push(county)
                return county
            }
        },

        addConstituency:{
            type: constituencyType,
            description:"Add a constituency",
            args: {
                id: { type: GraphQLNonNull(GraphQLInt) },
                constituency: { type: GraphQLNonNull(GraphQLString) },
                population: { type: GraphQLInt}
            },
            resolve: (parent, args) => {
                const constituency = { id: constituencies.length + 1, constituency: args.constituency, population: args.population}
                constituencies.push(constituency)
                return constituency
            }
        }
    })
})

const schema = new GraphQLSchema({
    query: rootQueryType,
    mutation: rootMutationType
    
})

app.use('/graphql', expressGraphql({
    schema: schema,
    graphiql:true //to give us a ui to access graphql instead of manually manipulating it through postman

}))
app.listen(5000, () => console.log('Server running'));